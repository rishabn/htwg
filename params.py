"""
Configuration file
"""

# Path to local folder that will hold all generated content
local_publisher_path = "./local"

# Path to webserver folder that will hold all published content
# Must have password-less access to this machine/folder
web_publisher_path = "240:~/htwg/"
html_filename = "sample.html"

# Files to include in analysis (takes wildcards only, no binaries!)
include_files = ["*.tex"]

# Files to explicitly exclude from analysis (takes only specific names)
exclude_files = ["packages.tex", "macros.tex"]

# List of words you want to avoid using while writing
# Credit to Matt Might (http://matt.might.net/articles/shell-scripts-for-passive-voice-weasel-words-duplicates/)
weasel_words = ["many", "various", "very", "fairly", "several", "extremely", "exceedingly", "quite", "remarkably",
               "few", "surprisingly", "mostly", "largely", "huge", "tiny", "excellent", "interestingly",
               "significantly", "substantially", "clearly", "vast", "relatively", "completely"]
passive_prefixes = ["am","are","were","being","is","been","was","be"]
passive_words = ["awoken", "been", "born", "beat", "become", "begun", "bent", "beset", "bet", "bid", "bidden", "bound",
                "bitten", "bled", "blown", "broken", "bred", "brought", "broadcast", "built", "burnt", "burst",
                "bought", "cast", "caught", "chosen", "clung", "come", "cost", "crept", "cut", "dealt", "dug", "dived",
                "done", "drawn", "dreamt", "driven", "drunk", "eaten", "fallen", "fed", "felt", "fought", "found",
                "fit", "fled", "flung", "flown", "forbidden", "forgotten", "foregone", "forgiven", "forsaken",
                "frozen", "gotten", "given", "gone", "ground", "grown", "hung", "heard", "hidden", "hit", "held",
                "hurt", "kept", "knelt", "knit", "known", "laid", "led", "leapt", "learnt", "left", "lent", "let",
                "lain", "lighted", "lost", "made", "meant", "met", "misspelt", "mistaken", "mown", "overcome",
                "overdone", "overtaken", "overthrown", "paid", "pled", "proven", "put", "quit", "read", "rid",
                "ridden", "rung", "risen", "run", "sawn", "said", "seen", "sought", "sold", "sent", "set", "sewn",
                "shaken", "shaven", "shorn", "shed", "shone", "shod", "shot", "shown", "shrunk", "shut", "sung",
                "sunk", "sat", "slept", "slain", "slid", "slung", "slit", "smitten", "sown", "spoken", "sped", "spent",
                "spilt", "spun", "spit", "split", "spread", "sprung", "stood", "stolen", "stuck", "stung", "stunk",
                "stridden", "struck", "strung", "striven", "sworn", "swept", "swollen", "swum", "swung", "taken",
                "taught", "torn", "told", "thought", "thrived", "thrown", "thrust", "trodden", "understood", "upheld",
                "upset", "woken", "worn", "woven", "wed", "wept", "wound", "won", "withheld", "withstood", "wrung",
                "written"]
passive = [str(x) + " " + str(y) for x in passive_prefixes for y in passive_words]
warning_words = weasel_words + passive

# Gnuplot template
plot_template = ""
plot_template += "set size 1,.85\n"
plot_template += "set terminal png #font \"Gill Sans,7\" linewidth 4 rounded fontscale .850\n"
plot_template += "set style line 80 lt rgb \"#808080\"\n"
plot_template += "set style line 81 lt 0\n"
plot_template += "set style line 81 lt rgb \"#808080\"\n"
plot_template += "set grid back linestyle 81\n"
plot_template += "set border 3 back linestyle 80\n"
plot_template += "set xtics nomirror\n"
plot_template += "set ytics nomirror\n"
plot_template += "set style line 1 lw 2 pt -1\n"
plot_template += "set style line 2 lw 2 pt -1\n"
plot_template += "set style line 3 lw 2 pt -1\n"
plot_template += "set style line 4 lw 2 pt -1\n"
plot_template += "set style line 5 lw 2 pt -1\n"
plot_template += "set style line 6 lw 2 pt -1\n"
plot_template += "set style line 7 lw 2 pt -1\n"
plot_template += "set style line 8 lw 2 pt -1\n"
plot_template += "set style line 9 lw 2 pt -1\n"
plot_template += "set style line 10 lw 2 pt -1\n"
plot_template += "set style line 11 lw 2 pt -1\n"
plot_template += "set style line 12 lw 2 pt -1\n"
plot_template += "set key outside horizontal center top samplen 1 spacing 1 width -.5\n"
plot_template += "set xtics border in scale 0,0 nomirror rotate by 45 offset character -1, -2, 1\n"

