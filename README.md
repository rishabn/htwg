# HTWG #

HTWG (How's the writing going?) is a script to track your writing progress. Every time you run the HTWG push script, it does the following:

* Cleans your source folder and compiles your LaTex documents.
* Updates a chart which tracks the number of words added to each document since the first use of HTWG.
* Searches your documents for cases of weasel words and passive voice.
* Generates word clouds for each document you want to monitor.
* Commits and pushes your updates to your Git repository.
* Assimilates all the charts, images, and warnings it generated into a single simple HTML file.
* Copies this HTML file to your webserver. 


## Required Packages ##

You need to have **gnuplot**, **yattag**, and **wordcloud** installed.

* gnuplot (version 4.6+) is used to generate the graph showing progress.
* yattag is used to generate the HTML page.
* wordcloud is used to generate the wordclouds for each document.

```
#!bash
$ apt-get install gnuplot
$ pip install yattag wordcloud
```

## Setting up HTWG ##

Clone this repository and copy over Makefile, push.py, and params.py files to any/all of your writing projects.

```
#!bash
$ git clone git@bitbucket.org:rishabn/htwg.git
$ cp *.py ~/papers/in-progress/thesis/.
```


## Setting project specific parameters ##

Open the params.py file that is local to your specific project. Below are the parameters that you NEED to change. The parameters that are not listed here are not likely to need changing (and if they do, they're very obvious).

###local_publisher_path###

```
#!python

local_publisher_path = "./local"
```

This is the folder that will house all the content generated by HTWG on your local machine. I recommend having a unique local folder for each project.

###web_publisher_path###

If you have a webserver that will host your HTWG reports, then pass the path to the folder that will host HTWG reports on your webserver. You need to have password-less access to this machine. [Here's how](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2).

```
#!python

web_publisher_path = "rishabn@240:~/htwg/thesis/"
```

If you don't want to publish HTWG reports on a webserver, just point this parameter to a folder on localhost.

```
#!python

web_publisher_path = "rishabn@localhost:~/htwg/thesis/"
```

###html_filename###

This is the name of the HTML report generated by HTWG. 
```
#!python

html_filename = "thesis-htwg-report.html"
```


## Running HTWG ##


```
#!bash

python push.py -cm "Commit message for your git commit/push"
```


## Viewing reports ##

If everything worked as expected, the HTML report should be present in both, your local and web publisher paths (that you set in the params.py file).

Here is a [sample report](http://130.245.177.240:8080/iclab-paper.html). There might only be single points on the chart because HTWG was only used for the last commit. The plots fill out as you keep using HTWG for your commits.

## FAQs ##

###What is this?###

This page is automatically updated everytime I commit and push to my dissertation repository. It tells you: (1) How much I've written, (2) How well I'm writing, and (3) What I'm writing about.

###Why did you make this? Dont you have "research" to do?###

When I started writing my dissertation, Christian Kreibich pointed me to the "[Thesis-o-meter](http://www.gousios.gr/sw/tom.html)". I couldn't find any working source code for it and I also I wanted to change a couple of things about how it worked, so I decided to procrastinate by writing this little bit of code. It's also a great way to pre-emptively answer the question:"How's the writing going, Rishab?"

###How does this work?###

After each commit and push, there is a little python script which automatically generates word counts and word clouds for all files I want it to. Then, it checks the text for use of weasel words and passive voice using a list I stole from [Matt Mights amazing blog](http://matt.might.net). If it finds sentences that I should reconsider, it puts them up on this webpage for the whole world to see (and shames you into writing better).

###Can I use this?###

Of course! Here is the source code: https://bitbucket.org/rishabn/htwg. If you find bugs, send me a message, but I cannot promise to fix anything. Better yet, fix them and send me a Pull Request!