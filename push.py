"""
Core logic.
This script generates statistics from source files and creates a publishable webpage.
"""

import params
import logging
import argparse
import sys
import glob
import os
import time
from yattag import Doc
from wordcloud import WordCloud, STOPWORDS
from subprocess import call


class Statistics:
    """
    Initializing Statistics will:
        1. Create a list of files on which stats will be computed (edit filters in params.py).
        2. Identify weasel words and passive voice usage in these files and write them to a log.
        3. Generate wordclouds and word counts for each of these files.
        4. Generate plots for tracking progress.
        5. Delete temp files.
        6. Generate the webpage.
    """

    def __init__(self):
        self.stats_path = params.local_publisher_path + "/stats/"
        self.create_stats_folder()
        self.files = self.generate_file_list()
        self.content = self.generate_content_list()
        self.generate_wordcloud()
        self.update_word_counts()
        self.plot_progress()
        self.find_poor_sentences()
        self.generate_webpage()

    def create_stats_folder(self):
        """
        Create a folder in which all generated stats and plots will be stored.
        :return:
        """
        if not os.path.exists(self.stats_path):
            os.makedirs(self.stats_path)

    def generate_file_list(self):
        """
        Generate a list of files that need to be analyzed (edit filters in params.py)
        :return: files: list of filenames
        """
        include, files = list(), list()
        for condition in params.include_files:
            include += glob.glob(condition)
        files = [x.split("/")[-1] for x in include]
        logging.debug("Files included in stats computation are: %s" % (' '.join(include)))
        exclude = [x.split("/")[-1] for x in params.exclude_files]
        for filename in files:
            if filename in exclude:
                files.remove(filename)
        logging.debug("Files to be analyzed: %s" % (' '.join(files)))
        return files

    def generate_content_list(self):
        """
        Generate a list containing lists of words in each included file.
        :return: file_content: A dict of lists.
                               {'file 1':["words", "in", "file 1"], ..., 'file n':["words", "in", "file n"]}
        """
        file_content = dict()
        file_content['all'] = list()
        for filename in self.files:
            with open(filename) as f:
                words = filter(lambda x: x[0] != '\\', f.read().split())
                file_content[filename] = words
                file_content['all'] += words
        print file_content.keys()
        return file_content

    def generate_wordcloud(self):
        """
        Write the word clouds from each file to disk.
        :return:
        """
        wc = WordCloud(background_color="white", max_words=2000, stopwords=STOPWORDS)
        for key in self.content.keys():
            try:
                wc.generate(' '.join(self.content[key]))
                image_name = str(key).split(".")[0] + "-cloud.png"
                wc.to_file(self.stats_path + image_name)
            except IndexError:
                logging.debug("Encountered an Index Error. Perhaps you documents are too short")

    def update_word_counts(self):
        """
        Write the current wordcounts for each file (and all files) to disk.
        :return:
        """
        total_words = 0
        for filename in self.content.keys():
            f = open(self.stats_path + "wordcount-" + filename.split(".")[0] + ".dat", "a")
            total_words += len(self.content[filename])
            string = str(time.time()) + "\t" + str(filename) + "\t" + str(len(self.content[filename])) + "\n"
            f.write(string)
            f.close()

    def plot_progress(self):
        """
        Generate a gnuplot chart showing word count progress in each file
        :return:
        """
        progress_template = params.plot_template
        progress_template += "set xlabel \"Day\"\n"
        progress_template += "set ylabel \"Words\"\n"
        progress_template += "set xdata time\n"
        progress_template += "set timefmt \"%s\"\n"
        progress_template += "set format x \"%m/%d\"\n"
        progress_template += "set output \"" + self.stats_path + "words-progress.png\"\n"
        progress_template += "plot "
        line_count = 0
        for key in self.content.keys():
            line_count += 1
            filename = self.stats_path + "wordcount-" + key.split(".")[0] + ".dat"
            progress_template += "\'" + filename + "\' using 1:3 w l ls " + str(line_count) \
                                 + " title \"" + key + "\""
            if line_count != len(self.content.keys()):
                progress_template += ", "
        f = open("temp", "w")
        f.write(progress_template)
        f.close()
        call(["gnuplot", "temp"])
        os.remove("temp")

    def find_poor_sentences(self):
        """
        Find all the poorly constructed lines in each file (lines that are in passive voice or use weasel words)
        Edit the list of words you want to avoid using in params.py
        :return:
        """
        w = open(self.stats_path + "warnings", "w")
        w.write("Filename: Line number | Line text | What's wrong?\n\n")
        for filename in self.files:
            string = ""
            f = open(filename)
            line_count = 0
            for line in f:
                line = line.strip()
                line_count += 1
                if any(word in line for word in params.warning_words):
                    for word in params.warning_words:
                        if word in line:
                            string = filename + ":" + str(line_count) + "\t|\t" + line + "\t|\t" + word + "\t|\n"
                    w.write(string)
            f.close()
        w.close()

    def generate_webpage(self):
        """
        Generate a HTML webpage that displays all the generated stats and maybe a little text
        :return:
        """
        doc, tag, text = Doc().tagtext()
        doc.asis('<!DOCTYPE html>')
        with tag('html'):
            with tag('body'):
                with tag('title'):
                    text("How's the writing going, Rishab?")
                with tag('h1'):
                    text("How's the writing going, Rishab?")
                doc.stag('hr')


                with tag('h2'):
                    text("Rishab's Progress (Last updated on %s)" % time.asctime(time.localtime()))

                with tag('h3'):
                    text('How much have I written?')
                    with tag('figure'):
                        doc.stag('img', src='./stats/words-progress.png', style="float:center;")
                    doc.stag('p', style="clear:both;")

                with tag('h3'):
                    text('What parts of my text should I consider rephrasing?')
                f = open(self.stats_path + "warnings")
                for line in f:
                    doc.text(line)
                    doc.stag("br")

                with tag('h3'):
                    text('What am I writing about?')
                with tag('p'):
                    text('These are the word clouds for each section/chapter')
                    doc.stag("br")
                    with tag('figure'):
                        for key in self.content.keys():
                            src = "./stats/" + str(key).split(".")[0] + "-cloud.png"
                            doc.stag('img', src=src,
                                     style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;")
                        doc.stag('p', style="clear:both;")
                doc.stag('hr')


                with tag('h2'):
                    text("FAQs")

                with tag('h3'):
                    text("What is this?")
                with tag('p'):
                    text("This page is automatically updated everytime I commit and push to my dissertation "
                         "repository. It tells you: (1) How much I've written, (2) How well I'm writing, and "
                         "(3) What I'm writing about.")

                with tag('h3'):
                    text("Why did you make this? Dont you have \"research\" to do?")
                with tag('p'):
                    text("When I started writing my dissertation, Christian Kreibich pointed me to the "
                         "\"Thesis-o-meter\" (see: http://www.gousios.gr/sw/tom.html). I couldn't find any "
                         "working source code for it and I also I wanted to change "
                         "a couple of things about how it worked, so I decided to procrastinate by writing "
                         "this little bit of code. It's also a great way to pre-emptively answer the question:"
                         "\"How's the writing going, Rishab?\"")

                with tag('h3'):
                    text("How does this work?")
                with tag('p'):
                    text("After each commit and push, there is a little python script which automatically "
                         "generates word counts and word clouds for all files I want it to. Then, it "
                         "checks the text for use of weasel words and passive voice using a list I stole "
                         "from Matt Mights amazing blog (see: http://matt.might.net). If it finds sentences "
                         "that I should reconsider, it puts them up on this webpage.")

                with tag('h3'):
                    text("Can I use this?")
                with tag('p'):
                    text("Of course! Here is the source code: https://bitbucket.org/rishabn/htwg. If you find bugs, send me a message, but I "
                         "cannot promise to fix anything.")
                doc.stag('hr')

        f = open(params.local_publisher_path + "/" + params.html_filename, "w")
        f.write(doc.getvalue())
        f.close()


class CheckIn:
    """
    Initializing CheckIn will:
        1. Compile your documents using the local Makefile
        2. Generate stats from the documents in your folder.
            These are stored in your local publisher path (edit path in params.py)
        3. Commit and push your updates to your Git repository.
        4. Copy the generated stats (as a webpage) to your webserver publisher path (edit path in params.py).
    Args:
        commit_message (str): Message to be used for the git commit.
    """

    def __init__(self, commit_message):
        self.make()
        self.generate_stats()
        self.push(commit_message)
        self.copy_content()

    def make(self):
        """
        Make clean and make the source folder.
        :return:
        """
        logging.info("Cleaning source and compiling.")
        call(["make", "clean"])
        call(["make"])

    def generate_stats(self):
        """
        Generate statistics about source documents.
        :return:
        """
        logging.info("Generating statistics.")
        Statistics()

    def push(self, message):
        """
        Git commit and push changes to Git tracked files.
        :param message: Message to be used while committing changes.
        :return:
        """
        logging.info("Committing source with log message: \"%s\" and pushing to repository." % message)
        call(["git", "commit", "-am", message])
        call(["git", "push", "origin", "master"])

    def copy_content(self):
        """
        Move generated statistics to remote web server.
        :return:
        """
        print os.getcwd()
        logging.info("Copying content to web server path: %s" % params.web_publisher_path)
        call(["scp", "-r", params.local_publisher_path + "/stats", params.web_publisher_path + "/stats"])
        call(["scp", "-r", params.local_publisher_path + "/" + params.html_filename, params.web_publisher_path])


class ArgumentParser:
    """
    Initializes a logger and handles passed parameters.
    Invalid parameters will display a help string and abort.
    """

    def __init__(self):
        self.args = self.__parse_args()
        root = logging.getLogger()
        if root.handlers:
            for handler in root.handlers:
                root.removeHandler(handler)
        if self.args.debug:
            logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(levelname)-8s %(filename)s:%(lineno)-4d: %(message)s',
                                datefmt='%m-%d %H:%M')
            logging.info("Debug level logging enabled")
        else:
            logging.basicConfig(level=logging.INFO,
                                format='%(asctime)s %(levelname)-8s %(filename)s:%(lineno)-4d: %(message)s',
                                datefmt='%m-%d %H:%M')
        logging.debug("Commit Message: %s" % self.args.commit_message)

    @staticmethod
    def __parse_args(print_help=False):
        """
        Handle command line arguments.
        :param print_help: Print help string by default when True.
        :return: parsed_args: Parsed command line arguments.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument('--debug', '-d', help='Enable debug logging', action='store_true')
        parser.add_argument('--commit-message', '-cm', help='Message to be used while committing changes',
                            dest='commit_message', required=True)
        parsed_args = parser.parse_args()
        if print_help:
            parser.print_help()
            sys.exit(0)
        return parsed_args


def main():
    ap = ArgumentParser()
    CheckIn(ap.args.commit_message)


main()
